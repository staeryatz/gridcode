# Introduction
The grid code command-line application manipulates a 16x16 grid based on file
input, and prints the number of cells containing the 'off' value.

# Important
I appreciate that in order to store 256 possible grid cells in just 32 bytes of
data, you need each of the 256 bits to be contiguous and used for bitpacking the
data structure efficiently. However, instead of using `unsigned char[32]` I fell
back on using the `std::bitset<256>`, which if you run sizeof() on, it does
equate to 32 bytes of memory used. I understand that the data type also contains
extra memory for its member functions compared to a plain 32-byte array.

I've tried bitpacking into the array and made (dangerous) attempts at pointer
arithmetic, but failed to use the syntax and the language properly to access the
bit addresses individually. I'd like to learn more but must also manage my time.

At the very least, the bitset is far superior to my first working iteration,
which used a `std::map<unsigned char, bool>` at a whopping 512 bytes of memory,
plus the added overhead of `std::map` functions. I'll take the ~8x improvement,
though I've failed to use the required data type at a small cost.

# Getting Started
This is an XCode C++ project. By default, if you run it directly from XCode, the
command-line parameter `input.dat` will be used. To use another input file, run
it directly from the command-line, or Edit the Scheme in the XCode project.

To generate `input.dat`, you may use `writeTestData()` in `main.cpp`, which is
commented out by default.

If you run in Debug, the application will also print out the grid to the screen.
To see only the result, build for Release and run the Release executable.
