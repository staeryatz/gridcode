//
//  main.cpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#include <string>
#include <fstream>
#include <iostream>
#include <filesystem>
#include <bitset>
#include "GridReader16x16.hpp"
#include "GridViewer16x16.hpp"
#include "IGrid.hpp"

using namespace GridCode;
namespace fs = std::__fs::filesystem;

void writeTestData()
{
	// Hack to provide input file. The input.dat is set as run argument
	// in the XCode -> Product -> Scheme by default if run from XCode.
	std::ofstream testData("input.dat", std::ios::out | std::ios::binary);
	char data[3] =
	{
		static_cast<char>(0xFF),
		static_cast<char>(0x11),
		static_cast<char>(0x12)
	};
	testData.write(data, 3);
	testData.close();
}

int main(int argc, const char * argv[])
{
	std::string filename = argv[1];

#ifdef DEBUG
//	writeTestData(); // Uncomment to generate sample input file
	fs::path fullPath = fs::path(fs::current_path()) / fs::path(filename);
	std::cout << "Input file path: " << fullPath << std::endl;
	std::cout << "Exists: " << fs::exists(filename) << std::endl;
	std::cout << "sizeof(bitset<256>): " << sizeof(std::bitset<256>) << std::endl;
#endif

    IGridReader* reader = new GridReader16x16();
    IGridViewer* viewer = new GridViewer16x16();
    IGrid* grid = reader->ReadGridFromFile(filename);

	viewer->PrintGrid(grid);

    delete reader;
    delete viewer;
    delete grid;

    return 0;
}
