//
//  IGrid.hpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#ifndef IGrid_h
#define IGrid_h

#include <stddef.h>
#include <stdint.h>

namespace GridCode
{
	class IGrid
	{
	public:
		virtual ~IGrid() {};

		virtual bool GetCellValue(const uint8_t row, const uint8_t col) const = 0;
		virtual unsigned int GetNumRows() = 0;
		virtual unsigned int GetNumCols() = 0;

		virtual void SaveValue(const unsigned char value) = 0;
	};
}

#endif /* IGrid_h */
