//
//  GridViewer16x16.cpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#include "GridViewer16x16.hpp"
#include "IGrid.hpp"

#include <iostream>

namespace GridCode
{
	GridViewer16x16::GridViewer16x16()
	{
	}

	GridViewer16x16::~GridViewer16x16()
	{
	}

	void GridViewer16x16::PrintGrid(IGrid* grid)
	{
		auto rows = grid->GetNumRows();
		auto cols = grid->GetNumCols();

		unsigned int zeroCount = 0;

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				bool value = grid->GetCellValue(i, j);
				if (!value)
				{
					zeroCount++;
				}

#ifdef DEBUG
				std::cout << " " << value << " ";
#endif
			}
#ifdef DEBUG
				std::cout << std::endl;
#endif
		}

		std::cout << zeroCount << std::endl;
	}
}
