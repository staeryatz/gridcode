//
//  Grid16x16.hpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#ifndef Grid16x16_hpp
#define Grid16x16_hpp

#include "IGrid.hpp"
#include <bitset>

namespace GridCode
{
	class Grid16x16 : public IGrid
	{
	public:
		Grid16x16();
		~Grid16x16();

		bool GetCellValue(const uint8_t row, const uint8_t col) const;
		unsigned int GetNumRows();
		unsigned int GetNumCols();

		void SaveValue(const unsigned char value);

	private:
		void CascadeUpdate(const unsigned char);
		void FlipCellValuesAdjacent(const uint8_t row, const uint8_t col);
		void FlipCellValuesDiagonal(const uint8_t row, const uint8_t col);
		void SetCellValue(const uint8_t row, const uint8_t col, const bool flag);

		void UnpackRowColIndices(
			const unsigned char c, uint8_t &out_row, uint8_t &out_col) const;
		unsigned char PackRowColIndices(const uint8_t row, const uint8_t col) const;

	private:
		//unsigned char mGridData[32]; // TODO: important use this data structure
		std::bitset<256> mGridBits;
	};
}
#endif /* Grid16x16_hpp */
