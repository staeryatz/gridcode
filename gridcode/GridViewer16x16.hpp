//
//  GridViewer16x16.hpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#ifndef GridViewer16x16_hpp
#define GridViewer16x16_hpp

#include "IGridViewer.hpp"

namespace GridCode
{
	class GridViewer16x16 : public IGridViewer
	{
		public:
			GridViewer16x16();
			~GridViewer16x16();
			void PrintGrid(IGrid* grid);
	};
}

#endif /* GridViewer16x16_hpp */
