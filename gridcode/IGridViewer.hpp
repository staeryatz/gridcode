//
//  IGridViewer.hpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#ifndef IGridViewer_h
#define IGridViewer_h

namespace GridCode
{
	class IGrid;

	class IGridViewer
	{
		public:
			virtual ~IGridViewer() {}
			virtual void PrintGrid(IGrid* grid) = 0;
	};
}
#endif /* IGridViewer_h */
