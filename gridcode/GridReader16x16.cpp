//
//  GridReader.cpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#include "GridReader16x16.hpp"
#include "Grid16x16.hpp"

#include <iostream>

namespace GridCode
{
	GridReader16x16::GridReader16x16() : mInputHandle()
	{
	}

	GridReader16x16::~GridReader16x16()
	{
	}

	IGrid* GridReader16x16::ReadGridFromFile(const std::string &filename)
	{
		auto grid = new Grid16x16();
		size_t fileSize = 0;
		size_t bytesRead = 0;
		unsigned char currentByte;

		try
		{
			mInputHandle.open(filename, std::ios::in | std::ios::binary);
  			mInputHandle.seekg(std::ios::end);
			fileSize = mInputHandle.tellg();
			mInputHandle.seekg(std::ios::beg);

			// Read one byte at a time, to ensure each value propagates the
			// affected adjacent and diagonal values on the grid.
			while (bytesRead <= fileSize)
			{
				mInputHandle.read(reinterpret_cast<char*>(&currentByte), 1);
				// basic_ifstream<unsigned char> requires providing explicit
				// specialization of character traits, and ifstream requires
				// a char* for read(). The best I've found after hours of
				// searching was to use ifstream and cast to unsigned char
				// afterwards, but none of the examples show this cast.
				// Still doesn't feel right...
				grid->SaveValue(currentByte);

				bytesRead++;
			}
		} catch (const std::exception &e) {
  			std::cerr << e.what() << std::endl;
		}

		mInputHandle.close();

		return grid;
	}
}
