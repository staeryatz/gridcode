//
//  IGridReader.hpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#ifndef IGridReader_h
#define IGridReader_h

#include <string>

namespace GridCode
{
	class IGrid;

	class IGridReader
	{
	public:
		virtual ~IGridReader() {}

		virtual IGrid* ReadGridFromFile(const std::string &filename) = 0;
	};
}

#endif /* IGridReader_h */
