//
//  Grid16x16.cpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#include "Grid16x16.hpp"

#define UNIFORM_SIZE 16

namespace GridCode
{
	Grid16x16::Grid16x16()
	{
    }

	Grid16x16::~Grid16x16()
	{
	}

	bool Grid16x16::GetCellValue(const uint8_t row, const uint8_t col) const
	{
		// We need to store 512 possible combinations of state in just 32 chars.
		// This can be done by using each bit in the 32 chars to represent a
		// boolean switch for each of the 256 cells, to be either 0 or 1.
		// In order to do this, we need to turn cell coords into memory coords.

		// Pseudo code:
		// 1. translate cell posistion into bit position in the array
		// 2. get the bit value at position in the array

		auto key = (int)PackRowColIndices(row, col);
		auto bit = mGridBits.test((size_t)key);

		return bit;
	}

	void Grid16x16::SetCellValue(
		const uint8_t row, const uint8_t col, const bool flag)
	{
		// We need to store 512 possible combinations of state in just 32 chars.
		// This can be done by using each bit in the 32 chars to represent a
		// boolean switch for each of the 256 cells, to be either 0 or 1.
		// In order to do this, we need to turn cell coords into memory coords.

		// Pseudo code:
		// 1. translate cell posistion into bit position in the array
		// 2. put the bit value at position in the array

		auto key = (int)PackRowColIndices(row, col);
		mGridBits.set(key, flag);
	}

	unsigned int Grid16x16::GetNumRows()
	{
		return UNIFORM_SIZE;
	}

	unsigned int Grid16x16::GetNumCols()
	{
		return UNIFORM_SIZE;
	}

	void Grid16x16::SaveValue(const unsigned char value)
	{
		CascadeUpdate(value);
	}

	void Grid16x16::CascadeUpdate(const unsigned char value)
	{
		// With each byte read, we want to do:
		//  1) Save the byte to the data structure
		//  2) Interpret which row + column the byte represents
		//  3) Flip the affected cells in the data structure

		uint8_t row;
		uint8_t col;

		UnpackRowColIndices(value, row, col);
		bool isOn = GetCellValue(row, col);

        if (isOn)
        {
			FlipCellValuesDiagonal(row, col);
		}
		else
		{
			FlipCellValuesAdjacent(row, col);
		}
	}

	void Grid16x16::FlipCellValuesAdjacent(uint8_t row, uint8_t col)
	{
		// Set adjacent cells in row and column to true
		for (int i = 0; i < UNIFORM_SIZE; i++)
		{
			for (int j = 0; j < UNIFORM_SIZE; j++)
			{
				if (i == row || j == col)
				{
					SetCellValue(i, j, true);
				}
			}
		}
	}

	void Grid16x16::FlipCellValuesDiagonal(uint8_t row, uint8_t col)
	{
		// TODO: JSB - There is probably some smart, bitshifting way to do this
		// with far less conditional checking, but this works for now...

		SetCellValue(row, col, false);

		// Set diagonals from row and column to true
		for (int i = 0; i < UNIFORM_SIZE; i++)
		{
			for (int j = 0; j < UNIFORM_SIZE; j++)
			{
				if (i > row && j > col)
				{
					if (i - row == j - col)
					{
						SetCellValue(i, j, false);
					}
				}
				else
				if (i < row && j < col)
				{
					if (i - row == j - col)
					{
						SetCellValue(i, j, false);
					}
				}
				else
				if (i > row && j < col)
				{
					if (i - row == col - j)
					{
						SetCellValue(i, j, false);
					}
				}
				else
				if (i < row && j > col)
				{
					if (row - i == j - col)
					{
						SetCellValue(i, j, false);
					}
				}
			}
		}
	}

	void Grid16x16::UnpackRowColIndices(
		const unsigned char byte, uint8_t &out_row, uint8_t &out_col) const
	{
		char highBits = byte & 15;
		char lowBits = byte >> 4;

		out_row = (uint8_t)lowBits;
		out_col = (uint8_t)highBits;
	}

	unsigned char Grid16x16::PackRowColIndices(
		const uint8_t row, const uint8_t col) const
	{
		unsigned char value = (char)row << 4;
		return value | (char)col;
	}
}
