//
//  GridReader16x16.hpp
//  gridcode
//
//  Created by Jeffrey Bakker on 2020-12-05.
//  Copyright © 2020 Jeffrey Bakker. All rights reserved.
//

#ifndef GridReader16x16_hpp
#define GridReader16x16_hpp

#include "IGridReader.hpp"
#include <fstream>

namespace GridCode
{
	class GridReader16x16 : public IGridReader
	{
	public:
		GridReader16x16();
		~GridReader16x16();

		IGrid* ReadGridFromFile(const std::string &filename);

	private:
		std::ifstream mInputHandle;
	};
}

#endif /* GridReader16x16_hpp */
